return {
  name='Balanced Annihilation',
  description='Balanced Annihilation',
  shortname='BA',
  version='V15.9.8',
  mutator='Official',
  game='Balanced Annihilation',
  shortGame='BA',
  modtype=1,
}